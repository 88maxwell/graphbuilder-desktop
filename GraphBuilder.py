from tkinter import *
from time import *
root=Tk()
C=Canvas(bg="grey80",height=root.winfo_reqheight()*2,width=root.winfo_reqwidth()*4)
LstCoord=[]     #List of coordinate of Vertex 
OneClickFlag=False  #variable what means that we choose vertex
NOfSelected=-1      #number of choosen vertex
matrix=[]       #matrix of connection 
r=12        #radius of vertex
def addToMatrix(matrix):#add one column and one row in connection matrix 
    matrix.append([])
    l=len(matrix)
    for i in range(l):
        matrix[l-1].append([0,0,0])
        if i!=l-1:
            matrix[i].append([0,0,0])
           
def deleter(event):     #function what delete vertex and all his connection with number=NOfSelected 
    if OneClickFlag:
        del LstCoord[NOfSelected]
        del matrix[NOfSelected]
        for i in range(len(matrix)):
            del matrix[i][NOfSelected]
        OCF()
        Number=len(matrix)
        ReDrawing()
    
def ReDrawing():        #function what redraw all object at canvas
    C.delete("all")
    for i in range(len(matrix)):
        for j in range(len(matrix)):
            if matrix[i][j][0]!=0:
                x1=LstCoord[i][0]
                y1=LstCoord[i][1]
                x2=LstCoord[j][0]
                y2=LstCoord[j][1]
                L=((x1-x2)**2+(y1-y2)**2)**(1/2)
                b=1-r/L
                x=x1+(x2-x1)*b
                y=y1-(y1-y2)*b
                line=C.create_line(x1,y1,x,y,width=2,arrow=LAST,tag="groupLine1")
                line=C.create_line(x1,y1,x2,y2,width=2,tag="groupLine1")
                weight=C.create_text((x1+x2)/2,(y1+y2)/2,text=matrix[i][j][2],font=("arial",18,"bold"),fill="red")
                matrix[i][j][1]=line
    for i in range(len(LstCoord)):
        x=LstCoord[i][0]
        y=LstCoord[i][1]
        oval=C.create_oval(x+r,y+r,x-r,y-r,fill="Yellow",tag="group1")
        text=C.create_text(x,y,text=str(i+1))
        LstCoord[i][2]=oval
        LstCoord[i][3]=text
    
def Select():       #function what changed color of choosen vertex
    if not(OneClickFlag):
        C.itemconfig(oval,fill="Green")
def OCF():      #function what changed value of variable OneClickFlag
    global OneClickFlag
    OneClickFlag=not(OneClickFlag)
    
def searchPosition(event):
    C.focus_set()
    selected=-1
    x=event.x
    y=event.y
    global NOfSelected
    if len(LstCoord)!=0:
        for i in range(len(LstCoord)):#search position where stay cursor at canvas 
            if ((LstCoord[i][0]+r>x and LstCoord[i][0]-r<x) and (LstCoord[i][1]+r>y and LstCoord[i][1]-r<y)):
                selected=i
        if selected!=-1:
            if (OneClickFlag) and NOfSelected!=selected:
                #__________________________________________________________ADD______________________________________________________________
                if  matrix[NOfSelected][selected][0]==0:
                    x1=LstCoord[NOfSelected][0]
                    y1=LstCoord[NOfSelected][1]
                    x2=LstCoord[selected][0]
                    y2=LstCoord[selected][1]
                    if matrix[selected][NOfSelected][0]==1:
                        weight=C.create_text((x1+x2)/2,(y1+y2)/2,text=matrix[selected][NOfSelected][2],font=("arial",18,"bold"),fill="red")
                    else:
                        weight=C.create_text((x1+x2)/2,(y1+y2)/2,text=entr.get(),font=("arial",18,"bold"),fill="red")
                    matrix[NOfSelected][selected][0]=1
                    matrix[NOfSelected][selected][2]=C.itemcget(weight,"text")
                else:
                    #__________________________________________________DELETE LINE______________________________________________________
                    matrix[NOfSelected][selected][0]=0
                    matrix[NOfSelected][selected][1]=0
                    matrix[NOfSelected][selected][2]=0
                ReDrawing()
                OCF()
            elif (OneClickFlag) and NOfSelected==selected:
                    C.itemconfig(LstCoord[selected][2],fill="Yellow")
                    OCF()  
            else:
                #_______________________________________________________CHOOSE VERTEX___________________________________________________________________
                C.itemconfig(LstCoord[selected][2],fill="Green")
                NOfSelected=selected
                
                OCF()
        else:
            if not(OneClickFlag):
                #_______________________________________________________CREATE VERTEX___________________________________________________________________
                a.CreateVertex(x,y)
            else:
                #___________________________________________________________MOVING______________________________________________________________________
                LstCoord[NOfSelected][0]=x
                LstCoord[NOfSelected][1]=y
                C.coords(LstCoord[NOfSelected][2],x-r,y-r,x+r,y+r)
                C.coords(LstCoord[NOfSelected][3],x,y)
                C.itemconfig(LstCoord[NOfSelected][2],fill="Yellow")
                OCF()
                ReDrawing()
    else:
        a.CreateVertex(x,y)
        
class Vertex():
    """
    x - x-coordinate
    
    """
    def __init__(self):
        self.x=None
        self.y=None
        
    def CreateVertex(self,x,y):
        addToMatrix(matrix)
        self.x=x
        self.y=y
        global oval
        oval=C.create_oval(self.x+r,self.y+r,self.x-r,self.y-r,fill="Yellow",tag="group1")
        text=C.create_text(x,y,text=str(len(matrix)))
        LstCoord.append([self.x,self.y,oval,text])
        
a=Vertex()
lbl=Label(root,text="Please, enter weight:",font="arial")
entr=Entry(root,font=("arial",16),width=20)
entr.insert(0,"1")
C.bind("<Button-1>",searchPosition)
C.bind("<Delete>",deleter)
lbl.grid(row=0,column=0,sticky=E)
entr.grid(row=0,column=1,sticky=W)
C.grid(row=1,column=0,columnspan=2)
root.mainloop()
